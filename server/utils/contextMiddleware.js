const jwt = require("jsonwebtoken");
const { PubSub } = require("apollo-server");

const pubsub = new PubSub();

module.exports = (context) => {
	let token;

	// If operation is a Query/Mutation
	if (context.req && context.req.headers.authorization) {
		// Obtain header-provided token from req.headers
		token = context.req.headers.authorization.split("Bearer ")[1];
	}
	// If operation is a Subscription
	else if (context.connection && context.connection.context.Authorization) {
		// Obtain connectionParams-provided token from connection.context
		token = context.connection.context.Authorization.split("Bearer ")[1];
	}

	token &&
		jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
			context.user = decodedToken;
		});

	context.pubsub = pubsub;

	return context;
};

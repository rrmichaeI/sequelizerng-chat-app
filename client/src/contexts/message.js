import React, { createContext, useContext, useReducer } from "react";

export const SET_USERS = "SET_USERS";
export const SET_USER_MESSAGES = "SET_USER_MESSAGES";
export const SET_SELECTED_USER = "SET_SELECTED_USER";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const ADD_REACTION = "ADD_REACTION";

const MessageStateContext = createContext();
const MessageDispatchContext = createContext();

const messageReducer = (state, action) => {
	let usersCopy, userIndex;
	const { username, message, messages, reaction } = action.payload;

	switch (action.type) {
		case SET_USERS:
			return {
				...state,
				users: action.payload,
			};
		case SET_USER_MESSAGES:
			usersCopy = [...state.users];

			userIndex = usersCopy.findIndex(
				(user) => user.username === username
			);

			usersCopy[userIndex] = {
				...usersCopy[userIndex],
				messages,
			};

			return {
				...state,
				users: usersCopy,
			};
		case SET_SELECTED_USER:
			usersCopy = state.users.map((user) => ({
				...user,
				selected: user.username === action.payload,
			}));

			return {
				...state,
				users: usersCopy,
			};
		case ADD_MESSAGE:
			usersCopy = [...state.users];

			userIndex = usersCopy.findIndex(
				(user) => user.username === username
			);

			message.reactions = [];

			let userCopyWithMessages = {
				...usersCopy[userIndex],
				messages: usersCopy[userIndex].messages
					? [message, ...usersCopy[userIndex].messages]
					: null,
				latestMessage: message,
			};

			usersCopy[userIndex] = userCopyWithMessages;

			return {
				...state,
				users: usersCopy,
			};
		case ADD_REACTION:
			// Make a shallow copy of the existing users
			usersCopy = [...state.users];

			// Get the index of the user that this reaction is of their messages
			userIndex = usersCopy.findIndex(
				(user) => user.username === username
			);

			// Make a shallow copy of user
			let userCopy = { ...usersCopy[userIndex] };

			// Find the index of the message that this reaction belongs to
			const messageIndex = userCopy.messages?.findIndex(
				(message) => message.uuid === reaction.message.uuid
			);

			// Check if such message exists
			if (messageIndex > -1) {
				// Make a shallow copy of user messages
				let messagesCopy = [...userCopy.messages];

				// Make a shallow copy of user message reactions
				let reactionsCopy = [...messagesCopy[messageIndex].reactions];

				// Find reaction by uuid
				const reactionIndex = reactionsCopy.findIndex(
					(r) => r.uuid === reaction.uuid
				);

				if (reactionIndex > -1) {
					// Reaction exists, update it
					reactionsCopy[reactionIndex] = reaction;
				} else {
					// New reaction, add it
					reactionsCopy = [...reactionsCopy, reaction];
				}

				messagesCopy[messageIndex] = {
					...messagesCopy[messageIndex],
					reactions: reactionsCopy,
				};

				userCopy = {
					...userCopy,
					messages: messagesCopy,
				};

				usersCopy[userIndex] = userCopy;
			}

			return {
				...state,
				users: usersCopy,
			};
		default:
			throw new Error(`Unknown action type: ${action.type}`);
	}
};

export const MessageProvider = ({ children }) => {
	const [state, dispatch] = useReducer(messageReducer, { users: null });

	return (
		<MessageDispatchContext.Provider value={dispatch}>
			<MessageStateContext.Provider value={state}>
				{children}
			</MessageStateContext.Provider>
		</MessageDispatchContext.Provider>
	);
};

export const useMessageState = () => useContext(MessageStateContext);
export const useMessageDispatch = () => useContext(MessageDispatchContext);

import React, { Fragment, useEffect, useState } from "react";
import { gql, useLazyQuery, useMutation } from "@apollo/client";
import { Col, Form } from "react-bootstrap";

import {
	SET_USER_MESSAGES,
	useMessageDispatch,
	useMessageState,
} from "../../contexts/message";
import Message from "./Message";

const GET_MESSAGES = gql`
	query getMessages($from: String!) {
		getMessages(from: $from) {
			uuid
			from
			to
			content
			createdAt
			reactions {
				uuid
				content
			}
		}
	}
`;

const SEND_MESSAGE = gql`
	mutation sendMessage($to: String!, $content: String!) {
		sendMessage(to: $to, content: $content) {
			uuid
			from
			to
			content
		}
	}
`;

export default function Messages() {
	const { users } = useMessageState();
	const dispatch = useMessageDispatch();
	const [content, setContent] = useState("");

	const selectedUser = users?.find((user) => user.selected === true);
	const messages = selectedUser?.messages;

	const [
		getMessages,
		{ loading: messagesLoading, data: messagesData },
	] = useLazyQuery(GET_MESSAGES);

	const [sendMessage] = useMutation(SEND_MESSAGE, {
		onError: (err) => console.log(err),
	});

	useEffect(() => {
		if (selectedUser && !selectedUser.messages) {
			getMessages({ variables: { from: selectedUser.username } });
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [selectedUser]);

	useEffect(() => {
		if (messagesData) {
			dispatch({
				type: SET_USER_MESSAGES,
				payload: {
					username: selectedUser.username,
					messages: messagesData.getMessages,
				},
			});
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [messagesData]);

	const submitMessage = (e) => {
		e.preventDefault();

		// If message is empty or no selected user
		if (content.trim() === "" || !selectedUser) return;

		setContent("");

		// Mutation for sending a message
		sendMessage({ variables: { to: selectedUser.username, content } });
	};

	let selectedChatMarkup;

	if (!messages && !messagesLoading) {
		selectedChatMarkup = <p className="info-text">Select a friend</p>;
	} else if (messagesLoading) {
		selectedChatMarkup = <p className="info-text">Loading...</p>;
	} else if (messages.length > 0) {
		selectedChatMarkup = messages.map((message, index) => (
			<Fragment key={message.uuid}>
				<Message message={message} />
				{index === messages.length - 1 && (
					<div className="invisible">
						<hr className="m-0" />
					</div>
				)}
			</Fragment>
		));
	} else if (messages.length === 0) {
		selectedChatMarkup = (
			<p className="info-text">
				You are now connected! Send your first message.
			</p>
		);
	}

	return (
		<Col xs={10} md={8} className="p-0">
			<div className="messages-box d-flex flex-column-reverse p-3">
				{selectedChatMarkup}
			</div>

			<div className="px-3 py-2">
				<Form onSubmit={submitMessage}>
					<Form.Group className="d-flex align-items-center m-0">
						<Form.Control
							type="text"
							className="message-input rounded-pill p-4 bg-secondary border-0"
							placeholder="Type a message..."
							value={content}
							onChange={(e) => setContent(e.target.value)}
						></Form.Control>

						<i
							className="fa fa-paper-plane fa-2x text-primary ml-2"
							aria-hidden="true"
							onClick={submitMessage}
							role="button"
						></i>
					</Form.Group>
				</Form>
			</div>
		</Col>
	);
}

import React, { useState } from "react";
import classNames from "classnames";
import { Button, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";
import moment from "moment";
import { gql, useMutation } from "@apollo/client";

import { useAuthState } from "../../contexts/auth";

const reactions = ["❤️", "😆", "😯", "😢", "😡", "👍", "👎"];

const REACT_TO_MESSAGE = gql`
	mutation reactToMessage($uuid: String!, $content: String!) {
		reactToMessage(uuid: $uuid, content: $content) {
			uuid
		}
	}
`;

export default function Message({ message }) {
	const { user } = useAuthState();
	const isSent = message.from === user.username;
	const isReceived = !isSent;
	const [showPopover, setShowPopover] = useState(false);

	// Show only distinct elements ie no double thumbs-up
	const reactionIcons = [
		...new Set(message.reactions.map((reaction) => reaction.content)),
	];

	const [reactToMessage] = useMutation(REACT_TO_MESSAGE, {
		onCompleted: (data) => setShowPopover(false),
		onError: (err) => console.log(err),
	});

	const react = (reaction) => {
		reactToMessage({
			variables: { uuid: message.uuid, content: reaction },
		});
	};

	const reactButton = (
		<OverlayTrigger
			trigger="click"
			placement="top"
			show={showPopover}
			onToggle={setShowPopover}
			transition={false}
			rootClose
			overlay={
				<Popover className="rounded-pill">
					<Popover.Content className="d-flex px-0 py-1 align-items-center react-button-popover">
						{reactions.map((reaction) => (
							<Button
								variant="link"
								className="react-icon-button"
								key={reaction}
								onClick={() => react(reaction)}
							>
								{reaction}
							</Button>
						))}
					</Popover.Content>
				</Popover>
			}
		>
			<Button variant="link" className="px-2">
				<i className="far fa-smile"></i>
			</Button>
		</OverlayTrigger>
	);

	return (
		<div
			className={classNames("d-flex my-3", {
				"ml-auto": isSent,
				"mr-auto": isReceived,
			})}
		>
			{/* If sent, react button at the left of the message */}
			{isSent && reactButton}

			<OverlayTrigger
				placement={isReceived ? "left" : "right"}
				overlay={
					<Tooltip>
						{moment(message.createdAt).format(
							"MMMM DD, YYYY @ h:mm a"
						)}
					</Tooltip>
				}
			>
				<div
					className={classNames(
						"py-2 px-3 rounded-pill position-relative",
						{
							"bg-primary": isSent,
							"bg-secondary": isReceived,
						}
					)}
				>
					{message.reactions.length > 0 && (
						<div className="reactions-div bg-secondary p-1 rounded-pill">
							{reactionIcons} {message.reactions.length}
						</div>
					)}
					<p
						className={classNames({
							"text-white": isSent,
							"text-dark": isReceived,
						})}
					>
						{message.content}
					</p>
				</div>
			</OverlayTrigger>

			{/* If received, react button at the right of the message */}
			{isReceived && reactButton}
		</div>
	);
}

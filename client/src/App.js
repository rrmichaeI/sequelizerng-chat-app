import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./App.scss";
import ApolloProvider from "./ApolloProvider";
import { AuthProvider } from "./contexts/auth";
import { MessageProvider } from "./contexts/message";
import DynamicRoute from "./utils/DynamicRoute";

import Home from "./pages/home/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";

function App() {
	return (
		<ApolloProvider>
			<AuthProvider>
				<MessageProvider>
					<Router>
						<Container className="pt-5">
							<Switch>
								<DynamicRoute
									exact
									path="/"
									component={Home}
									authenticated
								/>
								<DynamicRoute
									path="/register"
									component={Register}
									guest
								/>
								<DynamicRoute
									path="/login"
									component={Login}
									guest
								/>
								<Route
									render={() => (
										<h1 className="text-center">404</h1>
									)}
								/>
							</Switch>
						</Container>
					</Router>
				</MessageProvider>
			</AuthProvider>
		</ApolloProvider>
	);
}

export default App;
